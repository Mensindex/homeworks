package homework26;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


@DisplayName(value = "NumbersUtil is working when")
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
class NumbersUtilTest {

    private final NumbersUtil numbersUtil = new NumbersUtil();

    @Nested
    @DisplayName("gcd() is working")
    public class ForGcd {

        @ParameterizedTest(name = "return gcd {2} for {0} and {1}")
        @CsvSource(value = {"18, 12, 6", "9, 12, 3", "21, 14, 7"})
        public void return_correct_nod(int a, int b, int result) {
            assertEquals(result, numbersUtil.gcd(a, b));
        }

        @ParameterizedTest(name = "throws exception on {0} and {1}")
        @CsvSource(value = {"-5, 0", "15, -3", "0, 1"})
        public void bad_numbers_throws_exception(int badNumber1, int badNumber2) {
            assertThrows(IllegalArgumentException.class, () -> numbersUtil.gcd(badNumber1, badNumber2));
        }

    }




}