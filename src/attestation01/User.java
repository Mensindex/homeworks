package attestation01;

public class User {
    private  int id;
    private String name;
    private int age;
    private boolean existence;

    public User(int id, String name, int age, boolean existence) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.existence = existence;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isExistence() {
        return existence;
    }

    public void setExistence(boolean existence) {
        this.existence = existence;
    }
}
