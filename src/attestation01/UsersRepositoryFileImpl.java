package attestation01;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class UsersRepositoryFileImpl implements UserRepository {

    private String filename;

    public UsersRepositoryFileImpl(String filename) {
        this.filename = filename;
    }

    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        try (BufferedReader readAllUsers = new BufferedReader(new FileReader(filename))) {
            String line = readAllUsers.readLine();

            while (line != null) {
                String[] parts = line.split("\\|");
                int id = Integer.parseInt(parts[0]);
                String name = parts[1];
                int age = Integer.parseInt(parts[2]);
                boolean existence = Boolean.parseBoolean(parts[3]);
                User newUser = new User(id, name, age, existence);
                users.add(newUser);
                line = readAllUsers.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return users;
    }


    @Override
    public User findById(int id) {
        return findAll().stream()
                .filter(user -> id == user.getId())
                .findFirst().orElse(null);
    }


    @Override
    public void update(User user1) {
        List<User> users = findAll();
        for (int j = 0; j < users.size(); j++) {
            //User user = users.get(j);
            if (users.get(j).getId() == user1.getId()) {
                users.set(j, user1);
            }
        }
        try (BufferedWriter updateUser = new BufferedWriter(new FileWriter(filename))) {
            for (User user : users) {
                updateUser.write(user.getId() + "|" + user.getName() + "|" + user.getAge() + "|" + user.isExistence());
                updateUser.newLine();
            }
            updateUser.flush();
      } catch (IOException e) {
          throw new IllegalArgumentException(e);
      }
    }
}
