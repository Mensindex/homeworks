package attestation01;

public class Main {
    public static void main(String[] args) {

        UserRepository ourUsers = new UsersRepositoryFileImpl("people.txt");
        User user = ourUsers.findById(2);
        user.setName("Марсель");
        user.setAge(27);
        ourUsers.update(user);
        System.out.println(user.getName()+ " " + user.getAge());


    }
}
