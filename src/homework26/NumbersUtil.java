package homework26;

public class NumbersUtil {

    public int gcd (int x, int y) {
        if (x<=0 || y<=0) throw new IllegalArgumentException("Incorrect input");
        while(x!=0 && y!=0){
            if (x>y) x=x%y;
            else y=y%x;
        }
        return x+y;
    }
}
