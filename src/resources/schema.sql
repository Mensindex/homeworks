--Создание таблицы
create table account
(
  id         serial primary key,
  first_name varchar(20),
  last_name  varchar(20),
  age        integer check ( age > 0 and age < 120 )
);

--Добавление информации
insert into account(first_name, last_name, age)
values ('Марсель', 'Сидиков', 27);
insert into account(first_name, last_name, age)
values ('Виктор', 'Евлампьев', 25);
insert into account(first_name, last_name, age)
values ('Максим', 'Поздеев', 22);
insert into account(first_name, last_name, age)
values ('Разиль', 'Миниахметов', 24);
insert into account(first_name, last_name, age)
values ('Тимур', 'Хафизов', 24);

--Обновление информации
update account
set age = 28
where id = 1;

--Получение всех данных из таблицы
select *
from account;

--Получить только имена
select first_name
from account;

--Получить имя и возраст с сортировкой по возрастанию возраста
select first_name, age
from account
order by age;

--Получить имя и возраст с сортировкой по убыванию возраста
select first_name, age
from account
order by age desc;

--Получить всех, кто старше 25 лет
select *
from account
where age > 24;

--Сколько людей старше 24 лет
select count(*)
from account
where age > 24;

--Какие возраста втречаются
select distinct (age)
from account;

--Какие возраста и сколько раз втречаются
select age, count(*)
from account
group by age;

------------------
create table car
(
  id       serial primary key,
  model    varchar(20),
  color    varchar(20),
  owner_id integer,
  --Внешний ключ, ссылка на строку из другой таблицы
  foreign key (owner_id) references account (id)
);

insert into car (model, color, owner_id)
values ('BMW', 'Black', 5);
insert into car (model, color, owner_id)
values ('Lada', 'Green', 5);
insert into car (model, color, owner_id)
values ('Kia', 'White', 1);
insert into car (model, color, owner_id)
values ('Toyota', 'Black', 2);
insert into car (model, color, owner_id)
values ('Toyota', 'Aqua', 3);
insert into car (model, color)
values ('Bugatti', 'Blue');

--Получить имя пользователя с id 5 и количеством его машин
select first_name, (select count(*) from car where owner_id = 5) as cars_count
from account
where id = 5;

--Получить имена пользователей и количество их машин
select first_name, (select count(*) from car where owner_id = account.id) as cars_count
from account;

--Получить имена пользователей, у которых машины черного цвета
select first_name
from account
where account.id in (select owner_id from car where color = 'Black');

--Все владельцы и машины, у которых есть владельцы
select *
from account a
       left join car c on a.id = c.owner_id;

--Только владельцы и машины, которые есть друг у друга
select *
from account a
       inner join car c on a.id = c.owner_id;

--Все владельцы и машины
select *
from account a
       full join car c on a.id = c.owner_id;