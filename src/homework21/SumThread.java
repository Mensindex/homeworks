package homework21;
import static homework21.Main.array;
import static homework21.Main.sums;

public class SumThread extends Thread {
    private int index;
    private int from;
    private int to;

    public SumThread(int index, int from, int to) {
        this.index=index;
        this.from = from;
        this.to = to;

    }

    @Override
    public void run() {

        for (int i = from; i < to; i++) {
            sums[index]+=array[i];
        }
    }



}
