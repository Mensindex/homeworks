package homework21;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static int[] array;
    public static int[] sums;

    public static void main(String[] args) {

        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ведите размер входящего массива:");
        int numbersCount = scanner.nextInt();
        System.out.println("Введите количество потоков:");
        int threadsCount = scanner.nextInt();

        array = new int[numbersCount];
        sums = new int[threadsCount];

        //Заполняем случайными числами
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
        }

        int realSum = 0;

        for (int i = 0; i < array.length; i++) {
            realSum += array[i];
        }

        System.out.println("Результат без потоков: " + realSum);

        int from = 0;
        int to = array.length / threadsCount;
        Thread threads[] = new SumThread[threadsCount];
        for (int i = 0; i < threadsCount; i++) {
            threads[i] = new SumThread(i, from, to);
            threads[i].start();
            if (i != threadsCount - 1) {
                from = to;
                to += array.length / threadsCount;
            } else {
                from = to;
                to = array.length;
            }
        }

        for (int i = 0; i < threads.length; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                throw new IllegalArgumentException(e);
            }
        }

//
//        //TODO
//        Thread thread1 = new SumThread(0,0, array.length/threadsCount);
//        Thread thread2 = new SumThread(1,array.length / threadsCount, array.length / threadsCount + array.length / threadsCount);
//        Thread thread3 = new SumThread(2,array.length / threadsCount + array.length / threadsCount, array.length / threadsCount + array.length / threadsCount + array.length / threadsCount);
//        Thread thread4 = new SumThread(3,array.length / threadsCount + array.length / threadsCount + array.length / threadsCount, array.length);
//
//        thread1.start();
//        thread2.start();
//        thread3.start();
//        thread4.start();
//
//        try {
//            thread1.join();
//            thread2.join();
//            thread3.join();
//            thread4.join();
//        }catch (InterruptedException e){
//            throw new IllegalArgumentException(e);
//        }


        int byThreadsSum = 0;
        for (int i = 0; i < threadsCount; i++) {
            byThreadsSum += sums[i];
        }

        System.out.println("Результат с потоками: " + byThreadsSum);


    }
}
