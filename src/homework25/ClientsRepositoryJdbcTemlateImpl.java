package homework25;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;

public class ClientsRepositoryJdbcTemlateImpl implements ClientsRepository {

    //language=SQL
    private static final String SQL_INSERT_TO_CLIENT = "insert into client (first_name, last_name) values (?,?)";

    //language=SQL
    private static final String SQL_SELECT_ALL_CLIENTS = "select * from client order by id";

    private JdbcTemplate jdbcTemplate;

    public ClientsRepositoryJdbcTemlateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<Client> clientRowMapper = (row, rowNumber) -> {
        int id = row.getInt("id");
        String first_name = row.getString("first_name");
        String last_name = row.getString("last_name");
        return new Client(id, first_name, last_name);
    };

    @Override
    public List<Client> findAll() {

        return jdbcTemplate.query(SQL_SELECT_ALL_CLIENTS, clientRowMapper);
    }

    @Override
    public void save(Client client) {
        jdbcTemplate.update(SQL_INSERT_TO_CLIENT, client.getFirst_name(), client.getLast_name());
    }

}
