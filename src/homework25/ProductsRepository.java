package homework25;

import java.util.List;

public interface ProductsRepository {

    List<Product> findAll();

    List<Product> findAllByPrice(int price);

    List<Product> findAllByOrdersCount(int ordersCount);
}
