package homework25;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

public class Main {
    public static void main(String[] args) {

        DataSource dataSource = new DriverManagerDataSource("jdbc:postgresql://localhost:5432/pcs_3", "postgres", "1111");

        ClientsRepository clientsRepository = new ClientsRepositoryJdbcTemlateImpl(dataSource);

        System.out.println("Все клиенты: \n" + clientsRepository.findAll());

        ProductsRepository productsRepository = new ProductsRepositoryJdbcTemplateImpl(dataSource);
        System.out.println("Все товары: \n" + productsRepository.findAll());
        System.out.println("Все товары по искомой цене: \n" + productsRepository.findAllByPrice(350));
        System.out.println("все товары, которые заказывали искомое количество раз \n" + productsRepository.findAllByOrdersCount(2));

//        Client client = Client.builder()
//                .first_name("Mark")
//                .last_name("Down")
//                .build();


    }
}
