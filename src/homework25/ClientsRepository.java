package homework25;

import homework19.User;

import java.util.List;

public interface ClientsRepository {
    List<Client> findAll();
    void save(Client client);
}
