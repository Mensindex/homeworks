package homework25;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class Client {
    private int id;
    private String first_name;
    private String last_name;
}
