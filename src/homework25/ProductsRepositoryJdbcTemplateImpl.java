package homework25;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlTypeValue;

import javax.sql.DataSource;
import java.sql.SQLType;
import java.sql.Types;
import java.util.List;

public class ProductsRepositoryJdbcTemplateImpl implements ProductsRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL_PRODUCTS = "select * from product order by id";
    //language=SQL
    private static final String SQL_FIND_ALL_PRODUCT_BY_PRICE = "select * from product where price = ?";
    //language=SQL
    private static final String SQL_FIND_ALL_PRODUCTS_BY_ORDERS_COUNT = "select * from product where product.id in (select p.id\n" +
            "from product p\n" +
            "       inner join \"order\" o on p.id = o.product_id\n" +
            "group by p.id\n" +
            "having count(*) = ?)";

    private JdbcTemplate jdbcTemplate;

    public ProductsRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<Product> productRowMapper = (row, rowNumber) -> {
        int id = row.getInt("id");
        String description = row.getString("description");
        int price = row.getInt("price");
        int count = row.getInt("count");
        return new Product(id, description, price, count);
    };

    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL_PRODUCTS, productRowMapper);
    }

    @Override
    public List<Product> findAllByPrice(int price) {
        return jdbcTemplate.query(SQL_FIND_ALL_PRODUCT_BY_PRICE, new Object[]{price}, new int[]{Types.INTEGER}, productRowMapper);
    }

    @Override
    public List<Product> findAllByOrdersCount(int ordersCount) {
        return jdbcTemplate.query(SQL_FIND_ALL_PRODUCTS_BY_ORDERS_COUNT, new Object[]{ordersCount}, new int[]{Types.INTEGER}, productRowMapper);
    }
}
