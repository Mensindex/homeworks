package homework19;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class UsersRepositoryFileImpl implements UsersRepository {

    private String filename;

    public UsersRepositoryFileImpl(String filename) {
        this.filename = filename;
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<User>();
        //Объявили переменные для доступа
        Reader reader = null;
        BufferedReader bufferedReader = null;
        try {
            //Создали читалку на основе файла
            reader = new FileReader(filename);
            //Создали буферизированную читалку
            bufferedReader = new BufferedReader(reader);
            //Прочитали строку
            String line = bufferedReader.readLine();
            //Пока не пришла "нулевая строка"
            while (line != null) {
                //Разбиваем ее по |
                String[] parts = line.split("\\|");
                //Берем имя
                String name = parts[0];
                //Берем возраст
                int age = Integer.parseInt(parts[1]);
                //Берем статус о работе
                boolean isWorker = Boolean.parseBoolean(parts[2]);
                //Создаем нового человека
                User newUser = new User(name, age, isWorker);
                //Добавляем его в список
                users.add(newUser);
                //Считываем новую строку
                line = bufferedReader.readLine();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);

        } finally {
            //Этот блок выполнится точно
            if (reader != null) {
                try {
                    //Пытаемся закрыть ресурсы
                    reader.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedReader != null) {
                try {
                    //Пытаемся закрыть ресурсы
                    bufferedReader.close();
                } catch (IOException ignore) {
                }
            }
        }
        return users;
    }

    @Override
    public void save(User user) {
        Writer writer = null;
        BufferedWriter bufferedWriter = null;
        try {
            writer = new FileWriter(filename, true);
            bufferedWriter = new BufferedWriter(writer);
            bufferedWriter.write(user.getName() + "|" + user.getAge() + "|" + user.isWorker());
            bufferedWriter.newLine();
            bufferedWriter.flush();
        } catch (IOException e) {
            throw new IllegalArgumentException(e) {
            };
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException ignore) {
                }
            }
        }

    }

    @Override
    public List<User> findByAge(int age) {
        List<User> users = new ArrayList<User>();

        Reader reader = null;
        BufferedReader bufferedReader = null;

        try {
            reader = new FileReader(filename);
            bufferedReader = new BufferedReader(reader);
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                if (age == Integer.parseInt(parts[1])) {
                    String name = parts[0];
                    int age1 = Integer.parseInt(parts[1]);
                    boolean isWorker = Boolean.parseBoolean(parts[2]);
                    User ourUser = new User(name, age1, isWorker);
                    users.add(ourUser);
                }
                line = bufferedReader.readLine();
            }


        } catch (IOException e) {
            throw new IllegalArgumentException(e) {
            };
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ignore) {
                }
            }
        }
        return users;
    }


    @Override
    public List<User> findAllIsWorker() {
        List<User> ourUsers = new ArrayList<User>();
        Reader reader = null;
        BufferedReader bufferedReader = null;
        try {
            reader = new FileReader(filename);
            bufferedReader = new BufferedReader(reader);
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                if (Boolean.parseBoolean(parts[2])) {
                    String name = parts[0];
                    int age = Integer.parseInt(parts[1]);
                    boolean isWorker = Boolean.parseBoolean(parts[2]);
                    User ourUser = new User(name, age, isWorker);
                    ourUsers.add(ourUser);
                }
                line = bufferedReader.readLine();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e) {
            };
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ignore) {
                }
            }
        }

        return ourUsers;
    }
}
