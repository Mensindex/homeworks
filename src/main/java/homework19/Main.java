package homework19;

import java.util.List;

public class Main {
    public static void main(String[] args) {

        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");

//        List<User> users = usersRepository.findAll();
//
//        for (User user : users) {
//            System.out.println(user.getAge() + " " + user.getFirst_name() + " " + user.isWorker());
//        }
//
//        User user1 = new User("Алексей", 33, false);
//        User user2 = new User("Игорь", 33, true);
//        User user3 = new User("Владимир", 25, true);
//        User user4 = new User("Иван", 33, false);
//        usersRepository.save(user1);
//        usersRepository.save(user2);
//        usersRepository.save(user3);
//        usersRepository.save(user4);

        List<User> ourUsers = usersRepository.findByAge(33);
        if (!ourUsers.isEmpty()) {
            System.out.println("С искомым возрастом найдено (" + ourUsers.size() + ") человек:");
            for (User user : ourUsers) {
                System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
            }
        } else {
            System.out.println("С искомым возрастом никого не найдено");
        }

        System.out.println();

        List<User> whoIsWorker = usersRepository.findAllIsWorker();
        if (!whoIsWorker.isEmpty()) {
            System.out.println("Найдено (" + whoIsWorker.size() + ") работающих человек:");
            for (User user : whoIsWorker) {
                System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
            }
        } else {
            System.out.println("Работающих людей не найдено");
        }
    }
}
