package homework16;

public class LinkedList<T> {

    private static class Node<T> {
        T value;
        Node<T> next;

        public Node(T value) {
            this.value = value;
        }
    }

    private Node<T> first;
    private Node<T> last;
    private int size;

    public void add(T element) {
        Node<T> newNode = new Node<T>(element);
        if (size == 0) {
            first = newNode;
        } else {
            last.next = newNode;
        }
        last = newNode;
        size++;
    }

    public int getSize() {
        return size;
    }

    public T get(int index) {
        if (index >= 0 && index < size) {
            Node<T> temp = first;
            while (index != 0) {
                temp = temp.next;
                index--;

            }

            return temp.value;
        }else {
            System.err.println("Введенный индекс выходит за пределы list");
            return null;

        }
    }
}
