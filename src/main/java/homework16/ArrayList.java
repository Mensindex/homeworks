package homework16;

public class ArrayList<T> {
    private static final int DEFAULT_SIZE = 10;
    private T[] elements;
    private int size;

    public ArrayList() {
        this.elements = (T[]) new Object[DEFAULT_SIZE];
        this.size = 0;
    }

    public void add(T element) {
        if (size < elements.length) {
            this.elements[size] = element;
            size++;
        } else {
            System.err.println("Список переполнен");
        }
    }

    public void removeAt(int index) {
        if (index >= 0 && index < size) {
            for (int i = index; i < elements.length; i++) {
                if (i + 1 < elements.length) {
                    elements[i] = elements[i + 1];
                } else {
                    elements[i] = null;
                }
            }
        } else {
            System.err.println("Вводимый индекс выходит за размер массива");
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (T element : elements) {
            builder.append(element).append(" ");
        }
        return builder.toString();
    }
}
