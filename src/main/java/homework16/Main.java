package homework16;

public class Main {
    public static void main(String[] args) {

        ArrayList<Integer> numbers = new ArrayList<Integer>();
        numbers.add(50);
        numbers.add(23);
        numbers.add(7);
        numbers.add(456);
        numbers.add(345);
        numbers.add(75);
        numbers.add(82);
        numbers.add(3);
        numbers.add(728);
        numbers.add(0);

        numbers.removeAt(6);
        System.out.println(numbers.toString());


        LinkedList<Integer> list = new LinkedList<Integer>();
        list.add(34);
        list.add(120);
        list.add(-10);
        list.add(11);
        list.add(50);
        list.add(100);
        list.add(99);

        System.out.println(list.get(12));

    }
}
