package homework17;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {

        String text = "осень, в небе жгут корабли " +
                      "осень, мне бы прочь от земли " +
                      "там, где в море тонет печаль " +
                      "осень темная даль";


        String[] words = text.replaceAll("[^а-яА-Я ]", "").split(" ");


        Map<String, Integer> map = new HashMap<String, Integer>();

        for (String word : words) {
            if (!map.containsKey(word)) {
                map.put(word, 1);
            } else {
                map.put(word, map.get(word) + 1);
            }
        }

        for (String word : map.keySet()) {

            String key = word;
            String value = map.get(word).toString();
            System.out.println(key + " - " + value);
        }
    }
}
