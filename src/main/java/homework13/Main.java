package homework13;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Sequence sequence = new Sequence();

        ByCondition checkEvenNumber = new ByCondition() {
            public boolean isOk(int number) {
                boolean i = false;
                if (number % 2 == 0) {
                    i = true;
                }
                return i;
            }
        };

        ByCondition checkEvenSumOfDigits = new ByCondition() {
            public boolean isOk(int number) {
                boolean i = false;
                int temp = 0;

                while (number > 0) {
                    temp += number % 10;
                    number = number / 10;

                }
                if (temp % 2 == 0) {
                    i = true;
                }
                return i;
            }
        };

        int[] a = {569, 65, 841, 768, 78433, 6780, 77, 582, 207, 194};

        System.out.println("Initial array: " + Arrays.toString(a));

        System.out.println("Filter by even numbers: " +
                            Arrays.toString(sequence.filter(a, checkEvenNumber)));

        System.out.println("Filter by even sum of digits of a numbers: " +
                            Arrays.toString(sequence.filter(a, checkEvenSumOfDigits)));

    }
}
