package homework13;

import java.util.ArrayList;
import java.util.List;

public class Sequence {

    public int[] filter(int[] array, ByCondition condition) {

        List<Integer> temp = new ArrayList<Integer>();

        for (int i = 0; i < array.length; i++) {
            if (condition.isOk(array[i])) {
                temp.add(array[i]);
            }
        }

        int[] result = new int[temp.size()];
        for (int i = 0; i < temp.size(); i++) {
            result[i] = temp.get(i);
        }
        return result;
    }
}



