package homework20;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class MainByObject {
    public static void main(String[] args) {

        List<Car> cars = new ArrayList<>();
        try (BufferedReader readAllCars = new BufferedReader(new FileReader("input.txt"))) {

            String line = readAllCars.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                String number = parts[0];
                String name = parts[1];
                String color = parts[2];
                int milage = Integer.parseInt(parts[3]);
                int price = Integer.parseInt(parts[4]);
                Car newCar = new Car(number, name, color, milage, price);
                cars.add(newCar);
                line = readAllCars.readLine();
            }

            System.out.println("Авто черного цвета с нулевым пробегом:");
            cars.stream()
                    .filter(car -> car.getColor().equals("Black") || car.getMileage() == 0)
                    .forEach(car -> System.out.println(car.getName() + " с номером " + car.getNumber()));

            System.out.println();

            System.out.println("Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс.:");
            System.out.println(cars.stream()
                    .distinct()
                    .filter(car -> !(700000 > car.getPrice()) && !(800000 < car.getPrice()))
                    .count());

            System.out.println();

            System.out.println("Цвет автомобиля с минимальной стоимостью:");
            Car minCostCar = cars.stream()
                    .min(Comparator.comparingInt(car -> car.getPrice())).get();
            System.out.println(minCostCar.getColor());

            System.out.println();

            System.out.println("Средняя стоимость Camry:");
            System.out.println(cars.stream()
                    .filter(car -> car.getName().equals("Camry"))
                    .mapToInt(car -> car.getPrice()).average().getAsDouble());

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
