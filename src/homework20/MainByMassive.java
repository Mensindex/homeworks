package homework20;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;

public class MainByMassive {
    public static void main(String[] args) {

//        Function<String, String[]> wordsOfString = s -> {
//            String[] words = s.split("\\|");
//            return words;
//        };
        //Predicate<String[]> filterByColorAndMileage= words -> words[2].equals("Black") || words[3].equals("0");

        try (BufferedReader reader = new BufferedReader(new FileReader("input.txt"))) {

            System.out.println("Авто черного цвета с нулевым пробегом:");
            reader.lines()
                    .map(s -> {
                        String[] words = s.split("\\|");
                        return words;
                    })
                    .filter(words -> words[2].equals("Black") || words[3].equals("0"))
                    .forEach(words -> System.out.println(words[1] + " с номером " + words[0]));

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        System.out.println();

        try (BufferedReader reader1 = new BufferedReader(new FileReader("input.txt"))) {

            System.out.println("Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс.:");
            System.out.println(reader1.lines()
                    .distinct()
                    .map(s -> {
                        String[] words = s.split("\\|");
                        return words;
                    })
                    .filter(words -> !(700000 > Integer.parseInt(words[4])) && !(800000 < Integer.parseInt(words[4])))
                    .count());

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        System.out.println();

        try (BufferedReader reader2 = new BufferedReader(new FileReader("input.txt"))) {

            System.out.println("Цвет автомобиля с минимальной стоимостью:");
            String[] t = reader2.lines()
                    .map(s -> {
                        String[] words = s.split("\\|");
                        return words;
                    })
                    .min(Comparator.comparingInt(words -> Integer.parseInt(words[4]))).get();
            System.out.println(t[2]);

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        System.out.println();

        try (BufferedReader reader3 = new BufferedReader(new FileReader("input.txt"))) {

            System.out.println("Средняя стоимость Camry:");
            System.out.println(reader3.lines()
                    .map(s -> {
                        String[] words = s.split("\\|");
                        return words;
                    })
                    .filter(words -> words[1].equals("Camry"))
                    .mapToInt(words -> Integer.parseInt(words[4])).average().getAsDouble());

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }


    }
}
